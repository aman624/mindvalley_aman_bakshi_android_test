package com.aman.downloadlib;

import android.graphics.BitmapFactory;

import java.io.InputStream;

/**
 * Concrete class to support image downloads
 */
public class ImageDownloader extends Downloader {

    @Override
    protected Object processInputStream(InputStream inputStream) {
        return (inputStream != null) ? BitmapFactory.decodeStream(inputStream) : null;
    }
}
