package com.aman.downloadlib;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;
import android.util.LruCache;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Main class to manage downloads
 * This class provides APIs to initiate or cancel downloads
 * It manages multiple downloads in parallel
 * It notifies the caller when download is complete
 */
public class DownloadManager {
    public static final int DOWNLOAD_TYPE_IMAGE = 1;
    public static final int DOWNLOAD_TYPE_JSON = 2;

    public static final int DEFAULT_CACHE_SIZE = 4 * 1024 * 1024;
    public static final String TAG = "DownloadManager";

    private static DownloadManager sDownloadManager;
    private LruCache<String, Object> mMemoryCache;
    private List<onImageDownloadListener> mImageDownloadListeners;
    private List<onJSONDownloadListener> mJSONDownloadListeners;
    private List<onErrorListener> mErrorListeners;
    private Map<String, DownloadTask> mTaskMap;

    private DownloadManager() {
        mMemoryCache = new LruCache<>(DEFAULT_CACHE_SIZE);
        mImageDownloadListeners = new ArrayList<>();
        mJSONDownloadListeners = new ArrayList<>();
        mErrorListeners = new ArrayList<>();
        mTaskMap = new HashMap<>();
    }

    public static DownloadManager getInstance() {
        if (sDownloadManager == null) {
            synchronized (DownloadManager.class) {
                if (sDownloadManager == null) {
                    sDownloadManager = new DownloadManager();
                }
            }
        }
        return sDownloadManager;
    }

    public void download(Context context, String url, int downloadType) {
        DownloadTask task = new DownloadTask(context, url, downloadType);
        mTaskMap.put(url, task);
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void cancelDownload(String url) {
        DownloadTask task = mTaskMap.get(url);
        if (task != null) {
            task.cancel(true);
        }
    }

    public void setCacheLimit(int cacheSize) {
        mMemoryCache.resize(cacheSize);
    }

    public void clearCache() {
        mMemoryCache.evictAll();
    }

    public interface onImageDownloadListener {
        void onDownloadComplete(String url, Bitmap image);
    }

    public void registerImageDownloadListener(onImageDownloadListener listener) {
        mImageDownloadListeners.add(listener);
    }

    public void unregisterImageDownloadListener(onImageDownloadListener listener) {
        mImageDownloadListeners.remove(listener);
    }

    public interface onJSONDownloadListener {
        void onDownloadComplete(String url, String data);
    }

    public void registerJSONDownloadListener(onJSONDownloadListener listener) {
        mJSONDownloadListeners.add(listener);
    }

    public void unregisterJSONDownloadListener(onJSONDownloadListener listener) {
        mJSONDownloadListeners.remove(listener);
    }

    public interface onErrorListener {
        void onError(String url);
    }

    public void registerErrorListener(onErrorListener listener) {
        mErrorListeners.add(listener);
    }

    public void unregisterErrorListener(onErrorListener listener) {
        mErrorListeners.remove(listener);
    }

    private boolean isConnected(Context context) {
        ConnectivityManager connMgr = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return ((networkInfo != null && networkInfo.isConnected()));
    }

    private class DownloadTask extends AsyncTask<Void, Void, Object> {
        private Context context;
        private String url;
        private int downloadType;

        public DownloadTask(Context context, String url, int downloadType) {
            this.context = context;
            this.url = url;
            this.downloadType = downloadType;
        }

        @Override
        protected Object doInBackground(Void... voids) {
            Object object = mMemoryCache.get(url);
            if (object != null) {
                return object;
            }

            if (!isConnected(context)) {
                Log.e(TAG, "Internet not connected");
                return null;
            }

            Downloader downloader;
            if (downloadType == DOWNLOAD_TYPE_IMAGE) {
                downloader = new ImageDownloader();
            } else if (downloadType == DOWNLOAD_TYPE_JSON) {
                downloader = new JSONDownloader();
            } else {
                Log.w(TAG, "Unsupported download type");
                return null;
            }

            try {
                object = downloader.download(url);
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }

            if (isCancelled()) {
                Log.d(TAG, "Download cancelled");
                return null;
            }

            mMemoryCache.put(url, object);
            return object;
        }

        @Override
        protected void onPostExecute(Object o) {
            if (o == null) {
                for (onErrorListener listener : mErrorListeners) {
                    listener.onError(url);
                }
            } else if (downloadType == DOWNLOAD_TYPE_IMAGE) {
                for (onImageDownloadListener listener : mImageDownloadListeners) {
                    listener.onDownloadComplete(url, (Bitmap)o);
                }
            } else if (downloadType == DOWNLOAD_TYPE_JSON) {
                for (onJSONDownloadListener listener : mJSONDownloadListeners) {
                    listener.onDownloadComplete(url, (String)o);
                }
            }
            mTaskMap.remove(url);
        }
    }
}
