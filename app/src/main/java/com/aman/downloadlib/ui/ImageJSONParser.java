package com.aman.downloadlib.ui;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * JSON Parser to parse Image data retrieved from the server
 */
public class ImageJSONParser {
    private static final String TAG_URLS = "urls";
    private static final String TAG_THUMB = "thumb";

    public void parse(String json, List<Image> list) {
        if (json == null || list == null) {
            return;
        }

        try {
            JSONArray items = new JSONArray(json);
            for (int i = 0; i < items.length(); i++) {
                JSONObject item = items.getJSONObject(i);
                JSONObject urls = item.getJSONObject(TAG_URLS);
                String imageUrl = urls.getString(TAG_THUMB);
                Image image = new Image();
                image.imageUrl = imageUrl;
                list.add(image);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
