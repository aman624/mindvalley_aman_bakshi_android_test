package com.aman.downloadlib.ui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.aman.downloadlib.DownloadManager;
import com.aman.downloadlib.R;

import java.util.ArrayList;
import java.util.List;

public class ImageActivity extends AppCompatActivity implements DownloadManager.onErrorListener,
        DownloadManager.onJSONDownloadListener {

    private static final String URL = "http://pastebin.com/raw/wgkJgazE";

    private DownloadManager mDownloadManager;
    private ImageJSONParser mImageJSONParser;
    private List<Image> mImageList;

    private ProgressBar mProgressBar;
    private GridView mGridView;
    private ImageAdapter mImageAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);

        mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);
        mGridView = (GridView) findViewById(R.id.grid_view);

        mDownloadManager = DownloadManager.getInstance();
        mImageJSONParser = new ImageJSONParser();
        mImageList = new ArrayList<>();

        mImageAdapter = new ImageAdapter(this, mImageList);
        mGridView.setAdapter(mImageAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mDownloadManager.registerErrorListener(this);
        mDownloadManager.registerJSONDownloadListener(this);
        mImageAdapter.registerImageDownloadListener();

        mProgressBar.setVisibility(View.VISIBLE);
        mGridView.setVisibility(View.GONE);
        mImageList.clear();
        mDownloadManager.download(this, URL, DownloadManager.DOWNLOAD_TYPE_JSON);
    }

    @Override
    protected void onPause() {
        mDownloadManager.unregisterErrorListener(this);
        mDownloadManager.unregisterJSONDownloadListener(this);
        mImageAdapter.unregisterImageDownloadListener();
        super.onPause();
    }

    @Override
    public void onError(String url) {
        Toast.makeText(this, "Download error", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDownloadComplete(String url, String data) {
        mProgressBar.setVisibility(View.GONE);
        mGridView.setVisibility(View.VISIBLE);
        mImageJSONParser.parse(data, mImageList);
        mImageAdapter.notifyDataSetChanged();
    }
}
