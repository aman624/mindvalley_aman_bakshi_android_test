package com.aman.downloadlib.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.aman.downloadlib.DownloadManager;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Adapter to set downloaded images in the grid
 */
public class ImageAdapter extends BaseAdapter implements DownloadManager.onImageDownloadListener {
    private Context mContext;
    private List<Image> mImages;
    private Map<String, ImageView> mImageMap;
    private DownloadManager mDownloadManager;

    public ImageAdapter(Context c, List<Image> images) {
        mContext = c;
        mImages = images;
        mImageMap = new HashMap<>();
        mDownloadManager = DownloadManager.getInstance();
    }

    public int getCount() {
        return mImages.size();
    }

    public Object getItem(int position) {
        return mImages.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public void registerImageDownloadListener() {
        mDownloadManager.registerImageDownloadListener(this);
    }

    public void unregisterImageDownloadListener() {
        mDownloadManager.unregisterImageDownloadListener(this);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;
        if (convertView == null) {
            imageView = new ImageView(mContext);
            imageView.setLayoutParams(new GridView.LayoutParams(300, 300));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setPadding(8, 8, 8, 8);
        } else {
            imageView = (ImageView) convertView;
        }

        imageView.setBackgroundColor(Color.GRAY);
        Image image = (Image)getItem(position);
        imageView.setTag(image.imageUrl);
        mImageMap.put(image.imageUrl, imageView);
        mDownloadManager.download(mContext, image.imageUrl, DownloadManager.DOWNLOAD_TYPE_IMAGE);
        return imageView;
    }

    @Override
    public void onDownloadComplete(String url, Bitmap bitmap) {
        if (url == null || bitmap == null) {
            return;
        }

        ImageView imageView = mImageMap.get(url);
        if (imageView != null && url.equals(imageView.getTag())) {
            imageView.setBackgroundColor(Color.WHITE);
            imageView.setImageBitmap(bitmap);
        }
    }
}
