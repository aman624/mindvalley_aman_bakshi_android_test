package com.aman.downloadlib;

import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Abstract class for downloading
 * It requires a URL to download data
 * Currently JSON and Image data are supported
 */
public abstract class Downloader {
    public Object download(String urlString) throws IOException {
        URL url = new URL(urlString);
        HttpURLConnection connection = (HttpURLConnection)url.openConnection();
        connection.setDoInput(true);
        connection.connect();

        int response = connection.getResponseCode();
        if (response != HttpURLConnection.HTTP_OK) {
            Log.e(DownloadManager.TAG, "Download failed for " + urlString +
                    " with error: " + response);
            return null;
        }
        InputStream input = connection.getInputStream();
        return processInputStream(input);
    }

    protected abstract Object processInputStream(InputStream inputStream) throws IOException;
}
